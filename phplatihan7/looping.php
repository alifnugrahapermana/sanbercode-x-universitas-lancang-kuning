<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman looping</title>
</head>
<body>
    <h1>Soal looping</h1>
    <?php
    // soal 1
    echo "<h4>Soal No 1 Looping I Love PHP</h4>";
   
echo "LOOPING PERTAMA<br>";
for ($i = 2; $i <= 20; $i += 2) {
    echo "$i - I Love PHP<br>";
}

echo "<br>LOOPING KEDUA<br>";
for ($i = 20; $i >= 2; $i -= 2) {
    echo "$i - I Love PHP<br>";
}

// soal 2
echo "<h4>Soal No 2 Looping Array Modulo </h4>";
$numbers = [18, 45, 29, 61, 47, 34];
echo "array numbers: ";
print_r($numbers);
// Lakukan Looping di sini

$rest = []; // Inisialisasi array untuk menampung sisa bagi
foreach ($numbers as $number) {
    $remainder = $number % 5; // Hitung sisa bagi dengan 5
    $rest[] = $remainder; // Tambahkan sisa bagi ke dalam array $rest
}

echo "<br>";
echo "Array sisa baginya adalah:  "; 
echo "<br>";
print_r($rest);


// soal 3
echo "<h4>Soal No 3 Looping Asociative Array</h4>";
$items = [
    ['id' => '001', 'name' => 'Keyboard Logitek', 'price' => 60000, 'description' => 'Keyboard yang mantap untuk kantoran', 'source' => 'logitek.jpeg'], 
    ['id' => '002', 'name' => 'Keyboard MSI', 'price' => 300000, 'description' => 'Keyboard gaming MSI mekanik', 'source' => 'msi.jpeg'],
    ['id' => '003', 'name' => 'Mouse Genius', 'price' => 50000, 'description' => 'Mouse Genius biar lebih pinter', 'source' => 'genius.jpeg'],
    ['id' => '004', 'name' => 'Mouse Jerry', 'price' => 30000, 'description' => 'Mouse yang disukai kucing', 'source' => 'jerry.jpeg']
];

foreach ($items as $item) {
    echo "Array ( ";
    foreach ($item as $key => $value) {
        echo "[$key] => $value ";
    }
    echo ")\n";
    echo"<br>";
}
// soal 4
echo "<h4>Soal No 4 Asterix</h4>";
echo "Asterix: ";
echo "<br>";

for ($i = 1; $i <= 5; $i++) {
    for ($j = 1; $j <= $i; $j++) {
        echo "* ";
    }
    echo "<br>";
}

?>
</body>
</html>