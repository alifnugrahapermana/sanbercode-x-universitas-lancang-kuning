<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>halaman array</title>
</head>
<body>
        <h1>soal  array </h1>
        <?php
        // soal 1
        echo "<h4>soal 1</h4>";

        $kids = ["Mike", "Dustin", "Will", "Lucas", "Max", "Eleven"];
        $adults = ["Hopper", "Nancy",  "Joyce", "Jonathan", "Murray"];
        
        echo "Kids(" . implode(" ", array_map(function($key, $value) { return "[$key] => $value"; }, array_keys($kids), $kids)) . ")<br>";
        echo "Adults(" . implode(" ", array_map(function($key, $value) { return "[$key] => $value"; }, array_keys($adults), $adults)) . ")";
        
        // soal 2
        echo "<h4>soal 2</h4>";
        echo "Cast Stranger Things: ";
echo "<br>";
echo "Total Kids: " . count($kids); // Menampilkan panjang array kids
echo "<br>";
echo "<ol>";
foreach ($kids as $kid) {
    echo "<li>$kid</li>";
}
echo "</ol>";

echo "Total Adults: " . count($adults); // Menampilkan panjang array adults
echo "<br>";
echo "<ol>";
foreach ($adults as $adult) {
    echo "<li>$adult</li>";
}
echo "</ol>";


             // soal 3
        echo "<h4>soal 3</h4>";
        $data = array(
            array(
                "Name" => "Will Byers",
                "Age" => 12,
                "Aliases" => "Will the Wise",
                "Status" => "Alive"
            ),
            array(
                "Name" => "Mike Wheeler",
                "Age" => 12,
                "Aliases" => "Dugeon Master",
                "Status" => "Alive"
            ),
            array(
                "Name" => "Jim Hooper",
                "Age" => 43,
                "Aliases" => "Chief Hopper",
                "Status" => "Deceased"
            ),
            array(
                "Name" => "Eleven",
                "Age" => 12,
                "Aliases" => "El",
                "Status" => "Alive"
            )
        );
        echo "<pre>";
        echo "Array\n";
        echo "(\n";
        foreach ($data as $key => $item) {
            echo "\t[$key] => Array\n";
            echo "\t(\n";
            echo "\t\t[Name] => " . $item['Name'] . "\n";
            echo "\t\t[Age] => " . $item['Age'] . "\n";
            echo "\t\t[Aliases] => " . $item['Aliases'] . "\n";
            echo "\t\t[Status] => " . $item['Status'] . "\n";
            echo "\t)\n";
        }
        echo ")\n";
        echo "</pre>";
        
        ?>
</body>
</html>