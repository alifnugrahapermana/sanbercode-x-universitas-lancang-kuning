<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\Cast;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
      
        $cast = Cast::all();
        return view ('cast.tampil',['cast'=>$cast]);
 
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
       return view ('cast.tambah');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|string|min:5',
            'umur' => 'required|integer|min:0',
            'bio' => 'required|string',
        ],
        [
        'nama.required' => 'Nama cast wajib diisi',
        'nama.string' => 'Nama cast harus berupa teks',
        'nama.max' => 'Nama cast minimal 5karakter',
        'nama.unique' => 'Nama cast sudah ada, gunakan nama lain',
        'umur.required' => 'Umur wajib diisi',
        'umur.integer' => 'Umur harus berupa angka',
        'umur.min' => 'Umur tidak boleh negatif',
        'bio.required' => 'Bio wajib diisi',
        'bio.string' => 'Bio harus berupa teks',
    ]);

    $cast = new Cast;
    $cast->nama=$request->input('nama');
    $cast->umur=$request->input('umur');
    $cast->bio=$request->input('bio');
    $cast->save();
    return redirect('/cast');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $cast = Cast::find($id);
        

       return view ('cast.detail',['cast'=>$cast]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        
        $cast = Cast::find($id);

       return view ('cast.edit',['cast'=>$cast]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'nama' => 'required|string|min:5',
            'umur' => 'required|integer|min:0',
            'bio' => 'required|string',
        ],
        [
            'nama.required' => 'Nama cast wajib diisi',
            'nama.string' => 'Nama cast harus berupa teks',
            'nama.min' => 'Nama cast minimal 5 karakter',
            'umur.required' => 'Umur wajib diisi',
            'umur.integer' => 'Umur harus berupa angka',
            'umur.min' => 'Umur tidak boleh negatif',
            'bio.required' => 'Bio wajib diisi',
            'bio.string' => 'Bio harus berupa teks',
        ]);

        $cast = Cast::find($id);
        $cast->nama = $request->input('nama');
        $cast->umur = $request->input('umur');
        $cast->bio = $request->input('bio');
        $cast->save();

        return redirect('/cast');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
       Cast::where ('id',$id)->delete();
       return redirect ('/cast');
    }
}
