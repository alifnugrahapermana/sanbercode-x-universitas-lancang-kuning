@extends('layouts.master')

@section('title')
Halaman Tambah cast
@endsection
@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3>Tambah Cast</h3>
                </div>
                {{-- Validation --}}
                <form method="POST" action="/cast">
                  
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            @endif
                        @csrf
                            {{-- forminput --}}
                        <div class="form-group">
                            <label for="nama">Nama Cast</label>
                            <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan nama cast" required>
                        </div>

                        <div class="form-group">
                            <label for="umur">Umur</label>
                            <input type="number" class="form-control" id="umur" name="umur" placeholder="Masukkan umur" required>
                        </div>

                        <div class="form-group">
                            <label for="bio">Bio</label>
                            <textarea class="form-control" id="bio" name="bio" rows="3" placeholder="Masukkan bio" required></textarea>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
