<!DOCTYPE html>
<html lang="en">
<head>
    <?php $title ="Halaman String";?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?></title>
</head>
<body>
    <!-- soal 1 -->
    <h1>soal 1</h1>
    <?php
    // Contoh kode untuk menghitung panjang string dan jumlah kata
    
    $first_sentence = "Hello PHP!"; // Panjang string: 10, jumlah kata: 2
    $second_sentence = "I'm ready for the challenges"; // Panjang string: 28, jumlah kata: 5
    
    // Menghitung panjang string
    $length_first = strlen($first_sentence);
    $length_second = strlen($second_sentence);
    
    // Menghitung jumlah kata
    $words_first = str_word_count($first_sentence);
    $words_second = str_word_count($second_sentence);
    
    // Output hasil
    echo "<p>Kalimat 1: $first_sentence</p>";
    echo "<p>Panjang string pertama: $length_first, Jumlah kata: $words_first</p>";
    echo "<p>Kalimat 2: $second_sentence</p>";
    echo "<p>Panjang string kedua: $length_second, Jumlah kata: $words_second</p>";

    // soal 2
    echo "<h4> soal 2 </h4>";
    $string2 = "I love PHP";

    echo "<label>String: </label> \"$string2\" <br>";

    // Memisahkan kata-kata dalam string
    $words = explode(" ", $string2);

    // Mengambil dan menampilkan kata pertama
    echo "Kata pertama: " . $words[0] . "<br>";

    // Mengambil dan menampilkan kata kedua
    echo "Kata kedua: " . $words[1] . "<br>";

    // Mengambil dan menampilkan kata ketiga
    echo "Kata ketiga: " . $words[2] . "<br>";

    // soal3
    echo "<h4>soal 3 </h4>";

    $string3 = "PHP is old but sexy!";
    $new_string3 = str_replace("sexy", "awesome", $string3);
    
    echo "String soal 3: \"$string3\" <br>"; 
    echo "String soal 3 setelah diubah: \"$new_string3\"";
    ?>
    
</body>
</html>
