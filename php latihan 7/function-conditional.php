<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>halaman function-conditional </title>
</head>
<body>
    <h1>Soal function-conditional</h1>
    <?php
    // Soal 1
    echo "<h4>Soal No 1 Greetings</h4>";
    function greetings($name) {
        $formattedName = ucfirst(strtolower($name));
        echo "Halo $formattedName, Selamat Datang di Sanbercode!";
    }
    
    // Contoh penggunaan
    greetings("Bagas");
    echo "<br>";
    greetings("Wahyu");
    echo "<br>";
    greetings("nama peserta");
    echo "<br>";
    
    // Soal 2
    echo "<h4>Soal No 2 Reverse String</h4>";
    function reverseString($string) {
        $reversed = "";
        for ($i = strlen($string) - 1; $i >= 0; $i--) {
            $reversed .= $string[$i];
        }
        echo $reversed . "<br>";
    }
    
    // Hapus komentar di bawah ini untuk jalankan Code
    reverseString("abdul");
    reverseString("Sanbercode");
    reverseString("We Are Sanbers Developers");
    echo "<br>";
    

    // Soal 3
    echo "<h4>Soal No 3 Palindrome</h4>";

     // Definisikan fungsi balikkata()
     function balikkata($string) {
        $reversedString = '';
        $length = strlen($string);
        for ($i = $length - 1; $i >= 0; $i--) {
            $reversedString .= $string[$i];
        }
        return $reversedString;
    }

    // Definisikan fungsi palindrome()
    function palindrome($pali) {
        $balikkata2 =balikkata($pali);
        if($balikkata2 === $pali){
            echo "$pali => true <br>";
        }else{
            echo "$pali =>false <br>";
        }
    }

    // Contoh penggunaan
    echo palindrome("civic"); // true
    echo "<br>";
    echo palindrome("nababan"); // true
    echo "<br>";
    echo palindrome("jambaban"); // false
    echo "<br>";
    echo palindrome("racecar"); // true

    
    // Soal 4
    echo "<h4>Soal No 4 Tentukan Nilai</h4>";
   
    function tentukan_nilai($nilai) {
        if ($nilai >= 85 && $nilai <= 100) {
            return "$nilai => Sangat Baik";
        } elseif ($nilai >= 70 && $nilai < 85) {
            return "$nilai => Baik";
        } elseif ($nilai >= 60 && $nilai < 70) {
            return "$nilai => Cukup";
        } else {
            return "$nilai => Kurang";
        }
    }
    
    // Contoh penggunaan
    echo tentukan_nilai(98); // 98 => Sangat Baik
    echo "<br>";
    echo tentukan_nilai(76); // 76 => Baik
    echo "<br>";
    echo tentukan_nilai(67); // 67 => Cukup
    echo "<br>";
    echo tentukan_nilai(43); // 43 => Kurang
?>

   
</body>
</html>
